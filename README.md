This repo holds CRS definitions which aim to make it easier to use KSP data in
GIS software.


### `proj4/`

This directory contains `proj4`-style definitions, e.g.:

```
# Kerbin equirectangular
<30001> +proj=longlat +a=600000 +b=600000 +no_defs
```

### `proj4js/`

This directory contains a JSON file containing the `proj4`-style definitions, in a proj4js-friendly array, e.g.:

```
[
  ["KSP:31001",
    "+title=Kerbin equirectangular +proj=longlat +a=600000 +b=600000 +no_defs"]
]
```

### `wkt/`

This directory contains `WKT`-style definitions, in a [GeoServer-friendly format](http://docs.geoserver.org/latest/en/user/configuration/crshandling/customcrs.html), e.g.:

```
31001=GEOGCS["Kerbin", \
    DATUM["Kerbin", SPHEROID["Kerbin",600000,0]], \
    PRIMEM["Greenwich",0], \
    UNIT["degree",0.0174532925199433], \
    AXIS["Lon",EAST], AXIS["Lat",NORTH]]
```

## Legalese

```
"THE BEER-WARE LICENSE":
<ivan@sanchezortega.es> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.
```
